# XZ compressor
#export XZ_DEFAULTS='--memlimit=25%'
export XZ_OPT='-6v'

# TAR, list output of compressed TAR
alias tarlz4list='tar -I lz4 -tvf'
alias targzlist='tar -tvf'
alias tarxzlist='tar -tvf'

# defaults for `indent`
export SIMPLE_BACKUP_SUFFIX='.BAK'
alias indent='indent --linux-style -v'

alias listservices='systemctl --type=service'

# Start PHP live server
alias phplive='php -S localhost:8000'

# Go to mounted Windows folders
alias windows='cd /mnt/c/prg/_programming'

# Reload Zsh config (running source ~/.zshrc is deprecated)
alias reloadomz='omz reload'

# Default programs for file extensions
alias -s {php,ts,js,css,scss,c}="$EDITOR"
alias -s {jpg,jpeg,webp,png,gif,pcx,svg}='eog'

# GIT and SSH related
alias gits='git status --show-stash'
alias idssh='ssh-add ~/.ssh/introweb_ed25519'

# ENCRYPTION related
alias encrypt='ccencrypt --keyref ~/.config/ccrypt.keyref.cpt'
