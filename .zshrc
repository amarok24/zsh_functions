### BEGIN amarok zsh settings

# Exit script if not running as login shell, which prevents
# reloading .zshrc when another instance of zsh is launched by user.
[[ ! -o login ]] && return;

export PATH="$HOME/.local/bin:/usr/local/bin:/usr/sbin:/usr/bin:\
/usr/games:/usr/local/games"
#export MANPATH="/usr/local/man:$MANPATH"

# Bun TypeScript runtime
export BUN_INSTALL="$HOME/.local/bin/bun_runtime"
# Bun completions:
# [ -s "$HOME/.local/bin/bun_runtime" ]\
#&& source "$HOME/.local/bin/bun_runtime/_bun"

# Deno TypeScript runtime
export DENO_INSTALL="$HOME/.local/bin/deno_runtime"

# export JAVA_HOME='/usr/lib/jvm/java-19-openjdk-amd64'
# export PATH="$PATH:$JAVA_HOME/bin"

export LANG='en_US.utf8'
export LANGUAGE='en_US.utf8'
export LC_MESSAGES='POSIX'
export LC_MEASUREMENT='cs_CZ.utf8'
export LC_MONETARY='cs_CZ.utf8'
export LC_PAPER='cs_CZ.utf8'
# Fresh install: sudo dpkg-reconfigure locales, select en_US.utf8 + cs_CZ.utf8

export HISTSIZE=200
export SAVEHIST=3000
export EDITOR='micro'
# DEBIAN_VERSION=`cat /etc/debian_version` <-- old way to write it, new below:
# DEBIAN_VERSION=$(< /etc/debian_version)

if [[ $EUID -eq 0 ]]; then
	# root
	export EDITOR='nano'
fi
# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

EOL=$'\n'
RPROMPT='%F{75}[%T]%f'

if [[ $EUID -ne 0 ]]; then
	# home user
	PROMPT='%F{71}%n%f %F{75}[%~]%f${EOL}r%? $ '
else
	# root
	PROMPT='%S%F{160}%n%f%s@%m %F{75}[%d]%f${EOL}r%? # '
fi

### END amarok zsh settings


# Path to your oh-my-zsh installation.
ZSH="$HOME/.oh-my-zsh"

# ZSH automatic updates
# zstyle ':omz:update' mode disabled
# zstyle ':omz:update' mode auto
zstyle ':omz:update' mode reminder

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS="yyyy-mm-dd"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

if [[ $EUID -ne 0 ]]; then
	# home user
	plugins=(encode64 extract colored-man-pages)
else
	# root
	plugins=(encode64 extract)
fi

source $ZSH/oh-my-zsh.sh


if [[ -z $(pgrep -u $USER ssh-agent) ]]; then
	echo -n 'ssh-agent not running, starting '
    eval $(ssh-agent -s -t 9h)
else
	echo 'ssh-agent already running'
fi

source /etc/os-release
echo "\n=== ${PRETTY_NAME} ===\n"

ncal -MwbA1

test -f ~/.config/aliases.zsh && source ~/.config/aliases.zsh
test -f ~/.config/functions.zsh && source ~/.config/functions.zsh
test -f ~/.config/msts_functions.zsh && source ~/.config/msts_functions.zsh

# bun completions
[[ -s "$HOME/.local/bin/bun_runtime/_bun" ]] && source "$HOME/.local/bin/bun_runtime/_bun"
