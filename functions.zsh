# Conditional expressions in ZSH:
# https://zsh.sourceforge.io/Doc/Release/Conditional-Expressions.html


####################################
# CRC32C calculation of given file.
# RHash must be installed.
####################################
function crc32c_sum()
{
	if [[ -z $1 ]]; then
  		echo "Enter a filename to calculate CRC32C checksum."
  		echo "Checksum will be written to <filename>.crc32c"
  		echo "Use 'crc32c_verify' later to verify integrity."
 	  	return 1
	fi

	rhash --crc32c $1 > "$1.crc32c" && echo "OK! File '$1.crc32c' written."
}

#####################################
# CRC32C verification of given file.
# RHash must be installed.
#####################################
function crc32c_verify()
{
	if [[ -z $1 ]]; then
  		echo "Enter a filename to verify checksum."
  		echo "Same filename with 'crc32c' suffix must already exist."
  		echo "Use crc32c_sum to generate '.crc32c' file first."
 	  	return 1
	fi

	if [[ ! -f "$1.crc32c" ]]; then
  		echo "File '$1.crc32c' not found. Use 'crc32c_sum' to generate a checksum."
 	  	return 1
	fi

	rhash --crc32c --check "$1.crc32c"
}

alias crc32c_foldersum='rhash --crc32c --exclude=rhash -r . > crc32c.rhash'
alias crc32c_folderverify='rhash --check --crc32c crc32c.rhash'

function crc32c_folderupdate()
{
	# if folderverify fails then .rhash has to be deleted first
	rhash --check --crc32c -o /dev/null crc32c.rhash
	if [[ $? -ne 0 ]]; then
		# last return code != 0
		echo "Folder verification failed, check 'crc32c_folderverify' output,"
		echo "delete file 'crc32c.rhash' and rebuild hashes with 'crc32c_foldersum'"
		return 1
	fi
	rhash --crc32c --exclude=rhash -r . --update=crc32c.rhash
}

function compare_files()
{
	local f1
	local f2

	if [[ $# -ne 2 ]]; then
		echo "2 arguments (filenames) expected!"
		return 1
	fi
	
#	f1=`rhash --printf=%{crc32c} $1` <-- old way, new below
	f1=$(rhash --printf=%{crc32c} $1)
	f2=$(rhash --printf=%{crc32c} $2)

	# string comparison
	if [[ $f1 == $f2 ]]
	then
		echo "OK, identical."
		return 0
	else
		echo "The files are DIFFERENT."
		return 1
	fi
}
