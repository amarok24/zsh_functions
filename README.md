# zsh_functions

My useful functions, scripts and aliases for Z Shell (zsh).\
Most of it will probably work also in BASH.

## Usage
If you want to use any of the functions or aliases (from file `functions.zsh` or `aliases.zsh`),\
just put them directly into your `.zshrc` (or `.bashrc`) file.\
Alternatively you can put them into a filename and directory of your choice, for instance into\
`~/.config/myfunctions.zsh` and load them with the following command placed in `.zshrc`:

```sh
source ~/.config/myfunctions.zsh
```

License: The Unlicense\
May the source be with you :-)
